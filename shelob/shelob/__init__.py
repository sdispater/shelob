import re
import os
import requests
from flask import Flask, make_response, jsonify, request, abort
from rq import Queue
from rq.job import Job
from rq.exceptions import NoSuchJobError
from .jobs import crawl_urls

ERRORS = {
    'not_found': 'Not Found',
    'validation_failed': 'Validation failed'
}

URL_PATTERN = re.compile(
    r'^(?:http|ftp)s?://'  # http:// or https://
    # domain...
    r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
    r'localhost|'  # localhost...
    r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
    r'(?::\d+)?'  # optional port
    r'(?:/?|[/?]\S+)$', re.IGNORECASE)

app = Flask(__name__)

ENV = os.getenv('FLASK_ENV', 'development')


def load_config(environment):
    app.config.from_pyfile(
        os.path.join(os.path.dirname(os.path.abspath(__file__)), '../shelob/config', '%s.py' % environment))

# Loading configuration
load_config(ENV)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'message': ERRORS['not_found']}), 404)


def unprocessable_entity(message=None):
    return (
        make_response(
            jsonify({'message': message or ERRORS['validation_failed']}),
            422)
    )


def enqueue_job(job, *args):
    q = Queue('low', connection=app.config['REDIS'])

    return q.enqueue(job, *args)


def retrieve_job(job_id):
    try:
        return Job.fetch(job_id, connection=app.config['REDIS'])
    except NoSuchJobError:
        return abort(404)


@app.route('/jobs/create', methods=['POST'])
def jobs_create():
    # Validating input data

    # If Content-Type header is application/x-www-form-urlencoded, data will be empty.
    # Therefore, in the case data is empty we need to check "urls" for key.
    data = request.data

    if not data.strip():
        raw_urls = request.form.getlist('urls')

        if not raw_urls:
            # No urls provided
            return unprocessable_entity('Empty payload')
    else:
        # XXX bytes, bytes!
        raw_urls = data.decode("utf8").split("\n")

    urls = []
    for url in raw_urls:
        if not re.match(URL_PATTERN, url):
            # Invalid url
            return unprocessable_entity('url "%s" is invalid' % url)

        urls.append(url)

    job = enqueue_job(crawl_urls, urls)

    return jsonify({'id': job.id})


@app.route('/jobs/<job_id>/status', methods=['GET'])
def jobs_status(job_id):
    job = retrieve_job(job_id)

    return jsonify({'status': job.status})


@app.route('/jobs/<job_id>/result', methods=['GET'])
def jobs_result(job_id):
    job = retrieve_job(job_id)

    return jsonify({'result': job.result})


@app.route('/jobs/<job_id>/content/<path:url>', methods=['GET'])
def jobs_content(job_id, url):
    job = retrieve_job(job_id)

    if url not in job.result:
        abort(404)

    r = requests.get(url)
    response = make_response(r.content)
    response.headers['Content-Type'] = r.headers['Content-Type']

    return response


if __name__ == '__main__':
    app.run()
