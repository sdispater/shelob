# -*- coding: utf-8 -*-

try:
    # For Python 3.0 and later
    from urllib.request import urlopen  # noqa
    from urllib.parse import urlparse, urljoin  # noqa
except ImportError:
    # Fall back to Python 2's urllib2
    from urllib2 import urlopen  # noqa
    from urlparse import urlparse, urljoin  # noqa
