Shelob
======

Shelob is a small API which crawls urls and retrieve images from them.


## Installation

In the root of the project just do :

``` bash
pip install -r requirements.txt
```

It will install all necessary libraries to make Shelob work.

Copy all .sample files in config directory :

``` bash
find -type f -name '*.sample' | while read f; do mv "$f" "${f%.sample}"; done
```


## Workers

Starting a worker is quite simple:

``` bash
python shelob/worker.py low
```

By default, the environment is the development one, so if you want to
work in a specific environment you need to explicitly declare it via
the environment variable `FLASK_ENV`:

``` bash
FLASK_ENV=production python shelob/worker.py low
```


## Using the API

You can start the server with :

``` bash
python shelob/runserver.py
```

### Create a job

To create a job, just do:

``` bash
curl -X POST -d 'urls=http://www.cnn.com' -d 'urls=http://www.4chan.org' http://127.0.0.1:5000/jobs/create
```

It should return a job id:

``` javascript
{
    "id": "261748d2-e574-4520-98a2-4f06d6becdaf"
}
```

### Job status

To retrieve the job status, use the corresponding API:

``` bash
curl http://127.0.0.1:5000/jobs/261748d2-e574-4520-98a2-4f06d6becdaf/status
```

It should return a json with the status:

``` javascript
{
    "status": "finished"
}
```

### Job result

To retrieve the images urls found by the crawler:

``` bash
curl http://127.0.0.1:5000/jobs/261748d2-e574-4520-98a2-4f06d6becdaf/result
```

This should return a list of urls:

``` javascript
{
    "result": [
        "url1",
        "url2",
        // ...
    ]
}
```

### Retrieving the images

You can get the images retrieved by the crawler by calling:

``` bash
curl http://127.0.0.1:5000/jobs/261748d2-e574-4520-98a2-4f06d6becdaf/content/<image_url>
```

## Running the tests

To run the tests:

``` bash
nosetests -sv --rednose tests
```


