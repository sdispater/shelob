# -*- coding: utf-8 -*-

import json

from rq import Queue
from shelob import shelob
from . import ApiTestCase


def hello_world():
    return 'Hello world!'


class TestJobsResult(ApiTestCase):

    def test_valid_response(self):
        """
        /jobs/result should return a job result
        """
        q = Queue('low', connection=shelob.app.config['REDIS'], async=False)
        job = q.enqueue(hello_world)

        response = self.app.get('/jobs/%s/result' % job.id)

        self.assert_api_reponse(response)

        data = json.loads(response.data.decode("utf8"))
        self.assertTrue('result' in data)
        self.assertEqual(data['result'], 'Hello world!')

    def test_no_job(self):
        """
        /jobs/result should return a 404 when no job could be found
        """
        # No job found
        response = self.app.get('/jobs/not_an_id/status')

        self.assert_api_reponse(response, 404)
