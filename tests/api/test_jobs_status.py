# -*- coding: utf-8 -*-

import json

from . import ApiTestCase
from shelob import shelob


def hello_world():
    return 'Hello world!'


class TestJobsStatus(ApiTestCase):

    def test_valid_response(self):
        """
        /jobs/status should return a job status
        """
        job = shelob.enqueue_job(hello_world)

        response = self.app.get('/jobs/%s/status' % job.id)

        self.assert_api_reponse(response)

        data = json.loads(response.data.decode("utf8"))
        self.assertTrue('status' in data)

    def test_no_job(self):
        """
        /jobs/status should return a 404 when no job could be found
        """
        # No job found
        response = self.app.get('/jobs/not_an_id/status')

        self.assert_api_reponse(response, 404)
