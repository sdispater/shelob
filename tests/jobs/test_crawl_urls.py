# -*- coding: utf-8 -*-

from shelob.shelob.compat import urlparse
from tests import ShelobTestCase
from shelob.shelob.jobs import crawl_urls


def is_absolute_url(url):
    return bool(urlparse(url).scheme)


class CrawlUrlsTestCase(ShelobTestCase):

    def test_valid_response(self):
        """
        crawl_urls() should return a list of images urls
        """
        images = crawl_urls(['http://500px.com/popular'])

        self.assertTrue(isinstance(images, list))
        self.assertTrue(len(images) > 0)

    def test_absolute_urls(self):
        """
        crawl_urls() should return only absolute urls
        """
        images = crawl_urls(['http://www.sebastien-eustace.fr'])

        for image in images:
            self.assertTrue(is_absolute_url(image))
