# -*- coding: utf-8 -*-

from unittest import TestCase

from redis import StrictRedis

from shelob import shelob


class ShelobTestCase(TestCase):

    def find_empty_redis_database(self):
        """
        Tries to connect to a random Redis database (starting from 4), and
        will use/connect it when no keys are in there.
        """
        for dbnum in range(4, 17):
            testconn = StrictRedis(db=dbnum)
            empty = len(testconn.keys('*')) == 0
            if empty:
                return testconn

        assert False, 'No empty Redis database found to run tests in.'

    def setUp(self):
        shelob.load_config('test')
        shelob.app.config['REDIS'] = self.find_empty_redis_database()

    def tearDown(self):
        shelob.app.config['REDIS'].flushdb()
